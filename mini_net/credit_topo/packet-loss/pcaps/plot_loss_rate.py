import pyshark
import matplotlib.pyplot as plt
import numpy as np

pcap_files =['./pcaps/client_rtt60s_tcp20s_1.pcap', './pcaps/client_rtt60s_tcp20s_2.pcap', './pcaps/client_rtt60s_tcp20s_3.pcap', './pcaps/client_rtt60s_tcp20s_4.pcap']
lost_packets_list = []
total_packets_list = []

for file in pcap_files:
    print("Processing file: {}...".format(file))
    capture = pyshark.FileCapture(file)

    sequence_numbers = set()
    retransmissions = 0
    total_packets = 0
    lost_packets = 0

    for packet in capture:
        total_packets += 1

        if 'TCP' not in packet:
            continue

        if packet.ip.src != '160.16.11.100' or packet.ip.dst != '160.16.31.100':
            continue

        seq_num = int(packet.tcp.seq)

        if seq_num in sequence_numbers:
            retransmissions += 1
            lost_packets += 1
        else:
            sequence_numbers.add(seq_num)

    capture.close()

    print("File: {}".format(file))
    print("Total packets: {}".format(total_packets))
    print("Lost packets: {}".format(lost_packets))
    print("Retransmissions: {}".format(retransmissions))
    print("Loss rate: {}".format(lost_packets / total_packets))

    lost_packets_list.append(lost_packets)
    total_packets_list.append(total_packets)

loss_rates = np.divide(lost_packets_list, total_packets_list) * 100

print("Lost packets for each file: {}".format(lost_packets_list))
print("Loss rates for each file: {}".format(loss_rates))

plt.figure(figsize=(10, 7))
plt.xlabel("Packet Loss Rate (%)")
plt.ylabel("20_TCP")
plt.title("Packet Loss Rate for 4 Runs t=20s", fontsize=14, fontweight='bold')

boxes = plt.boxplot(loss_rates, vert=False, patch_artist=True, showfliers=False, medianprops={'color': 'red'})

for box in boxes['boxes']:
    box.set(color='brown', linewidth=2)
    box.set(facecolor='lightblue')
    box.set(hatch='/')

plt.show()
