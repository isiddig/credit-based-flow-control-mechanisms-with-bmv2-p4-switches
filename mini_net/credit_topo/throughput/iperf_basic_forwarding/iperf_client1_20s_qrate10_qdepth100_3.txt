Connecting to host h2, port 5201
[  7] local 160.16.11.100 port 55026 connected to 160.16.31.100 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  7]   0.00-1.00   sec   314 KBytes  2.57 Mbits/sec    0    101 KBytes       
[  7]   1.00-2.00   sec   507 KBytes  4.16 Mbits/sec    0    194 KBytes       
[  7]   2.00-3.00   sec  0.00 Bytes  0.00 bits/sec    0    221 KBytes       
[  7]   3.00-4.00   sec  0.00 Bytes  0.00 bits/sec    0    231 KBytes       
[  7]   4.00-5.00   sec   277 KBytes  2.27 Mbits/sec    0    249 KBytes       
[  7]   5.00-6.00   sec  0.00 Bytes  0.00 bits/sec    0    295 KBytes       
[  7]   6.00-7.00   sec  0.00 Bytes  0.00 bits/sec    0    341 KBytes       
[  7]   7.00-8.00   sec   277 KBytes  2.27 Mbits/sec    0    388 KBytes       
[  7]   8.00-9.00   sec  0.00 Bytes  0.00 bits/sec    0    434 KBytes       
[  7]   9.00-10.00  sec  0.00 Bytes  0.00 bits/sec    0    480 KBytes       
[  7]  10.00-11.00  sec  1.14 MBytes  9.60 Mbits/sec    0    517 KBytes       
[  7]  11.00-12.00  sec  0.00 Bytes  0.00 bits/sec    0    563 KBytes       
[  7]  12.00-13.00  sec  0.00 Bytes  0.00 bits/sec    0    618 KBytes       
[  7]  13.00-14.00  sec   720 KBytes  5.90 Mbits/sec    0    664 KBytes       
[  7]  14.00-15.00  sec  0.00 Bytes  0.00 bits/sec    0    710 KBytes       
[  7]  15.00-16.00  sec  0.00 Bytes  0.00 bits/sec    0    757 KBytes       
[  7]  16.00-17.00  sec   941 KBytes  7.71 Mbits/sec    0    803 KBytes       
[  7]  17.00-18.00  sec  0.00 Bytes  0.00 bits/sec    0    849 KBytes       
[  7]  18.00-19.00  sec  0.00 Bytes  0.00 bits/sec    0    895 KBytes       
[  7]  19.00-20.00  sec  0.00 Bytes  0.00 bits/sec    0    941 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  7]   0.00-20.00  sec  4.11 MBytes  1.72 Mbits/sec    0             sender
[  7]   0.00-30.90  sec  2.66 MBytes   722 Kbits/sec                  receiver

iperf Done.
