Connecting to host h2, port 5201
[  7] local 160.16.12.100 port 40538 connected to 160.16.31.100 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  7]   0.00-1.00   sec   314 KBytes  2.57 Mbits/sec    0   92.3 KBytes       
[  7]   1.00-2.00   sec   166 KBytes  1.36 Mbits/sec    0    129 KBytes       
[  7]   2.00-3.00   sec   185 KBytes  1.51 Mbits/sec    0    212 KBytes       
[  7]   3.00-4.00   sec  0.00 Bytes  0.00 bits/sec    0    221 KBytes       
[  7]   4.00-5.00   sec   148 KBytes  1.21 Mbits/sec    0    231 KBytes       
[  7]   5.00-6.00   sec  0.00 Bytes  0.00 bits/sec    0    258 KBytes       
[  7]   6.00-7.00   sec   683 KBytes  5.60 Mbits/sec    0    304 KBytes       
[  7]   7.00-8.00   sec  0.00 Bytes  0.00 bits/sec    0    351 KBytes       
[  7]   8.00-9.00   sec  0.00 Bytes  0.00 bits/sec    0    397 KBytes       
[  7]   9.00-10.00  sec  0.00 Bytes  0.00 bits/sec    0    443 KBytes       
[  7]  10.00-11.00  sec  1.14 MBytes  9.53 Mbits/sec    0    489 KBytes       
[  7]  11.00-12.00  sec  0.00 Bytes  0.00 bits/sec    0    535 KBytes       
[  7]  12.00-13.00  sec  0.00 Bytes  0.00 bits/sec    0    581 KBytes       
[  7]  13.00-14.00  sec  0.00 Bytes  0.00 bits/sec    0    618 KBytes       
[  7]  14.00-15.00  sec   775 KBytes  6.35 Mbits/sec    0    664 KBytes       
[  7]  15.00-16.00  sec  0.00 Bytes  0.00 bits/sec    0    710 KBytes       
[  7]  16.00-17.00  sec  0.00 Bytes  0.00 bits/sec    0    757 KBytes       
[  7]  17.00-18.00  sec  0.00 Bytes  0.00 bits/sec    0    803 KBytes       
[  7]  18.00-19.00  sec   996 KBytes  8.16 Mbits/sec    0    858 KBytes       
[  7]  19.00-20.00  sec  0.00 Bytes  0.00 bits/sec    0    904 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  7]   0.00-20.00  sec  4.32 MBytes  1.81 Mbits/sec    0             sender
[  7]   0.00-29.90  sec  2.65 MBytes   743 Kbits/sec                  receiver

iperf Done.
