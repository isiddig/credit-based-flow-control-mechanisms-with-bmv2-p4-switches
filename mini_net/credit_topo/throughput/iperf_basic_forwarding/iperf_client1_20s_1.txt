Connecting to host h2, port 5201
[  7] local 160.16.11.100 port 35140 connected to 160.16.31.100 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  7]   0.00-1.00   sec  42.3 MBytes   355 Mbits/sec    0   5.84 MBytes       
[  7]   1.00-2.00   sec   110 MBytes   923 Mbits/sec    0   8.40 MBytes       
[  7]   2.00-3.00   sec   111 MBytes   933 Mbits/sec    0   8.40 MBytes       
[  7]   3.00-4.00   sec  96.2 MBytes   807 Mbits/sec    0   8.40 MBytes       
[  7]   4.00-5.00   sec   110 MBytes   923 Mbits/sec    0   8.40 MBytes       
[  7]   5.00-6.00   sec  93.8 MBytes   786 Mbits/sec    7   6.30 MBytes       
[  7]   6.00-7.00   sec  91.2 MBytes   765 Mbits/sec    0   6.96 MBytes       
[  7]   7.00-8.00   sec  83.8 MBytes   703 Mbits/sec   14   5.27 MBytes       
[  7]   8.00-9.00   sec  78.8 MBytes   661 Mbits/sec    3   3.94 MBytes       
[  7]   9.00-10.00  sec  65.0 MBytes   545 Mbits/sec    0   4.26 MBytes       
[  7]  10.00-11.00  sec  70.0 MBytes   587 Mbits/sec    0   4.47 MBytes       
[  7]  11.00-12.00  sec  72.5 MBytes   608 Mbits/sec    0   4.60 MBytes       
[  7]  12.00-13.00  sec  73.8 MBytes   619 Mbits/sec    0   4.67 MBytes       
[  7]  13.00-14.00  sec  73.8 MBytes   619 Mbits/sec    0   4.69 MBytes       
[  7]  14.00-15.00  sec  76.2 MBytes   640 Mbits/sec    0   4.69 MBytes       
[  7]  15.00-16.00  sec  73.8 MBytes   619 Mbits/sec    0   4.69 MBytes       
[  7]  16.00-17.00  sec  76.2 MBytes   640 Mbits/sec    0   4.71 MBytes       
[  7]  17.00-18.00  sec  76.2 MBytes   640 Mbits/sec    0   4.79 MBytes       
[  7]  18.00-19.00  sec  75.0 MBytes   629 Mbits/sec    0   4.93 MBytes       
[  7]  19.00-20.00  sec  76.2 MBytes   640 Mbits/sec    4   4.57 MBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  7]   0.00-20.00  sec  1.59 GBytes   682 Mbits/sec   28             sender
[  7]   0.00-20.06  sec  1.58 GBytes   679 Mbits/sec                  receiver

iperf Done.
