# Credits Based Flow Control


<!-- 
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.mpi-klsb.mpg.de/isiddig/credits-based-flow-control.git
git branch -M main
git push -uf origin main
``` -->
<!-- 
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.mpi-klsb.mpg.de/isiddig/credits-based-flow-control/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html) -->

<!-- ## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

*** -->


## Description
The project mainly uses the programmable SDN switches to solve the problem of congestion control which caused by the flows in the network backbone. The project implements a technique that depends on the Credits assignment logic to push the bottleneck to the ingress switch which results in saving more bandwidth of the total capacity.

## Abstract

This work evaluates the effectiveness of credit-based flow control mechanisms by analyzing queue utilization in egress switches. Traffic congestion in packet-switched networks poses significant challenges, leading to increased latency and packet loss. Traditional flow control methods often fall short in managing high-volume, dynamic traffic effectively. To address this, a novel credit-based flow control mechanism is proposed, which regulates data packet transmission through a system of credits, each representing a specific number of packets. Implemented on P4 switches using the behavioral model bmv2, the mechanism dynamically allocates and adjusts credits based on real-time network conditions, ensuring that no single data flow can dominate network resources and cause congestion. The study measures the impact of this mechanism by monitoring queue usage at egress switches and comparing it with traditional flow control methods. Results indicate that the credit-based flow control mechanism apparently reduces congestion, improves packet delivery rates, and enhances overall network efficiency. This study provides valuable insights into the potential of credit-based flow control to optimize traffic management and improve the performance of modern packet-switched networks.

## Topology

<img src="https://gitlab.mpi-klsb.mpg.de/isiddig/credit-based-flow-control-mechanisms-with-bmv2-p4-switches/-/raw/main/images/topology__1_.png" align="center" height="350" width="800"/>




<!-- [slides](https://gitlab.mpi-klsb.mpg.de/isiddig/credit-based-flow-control-mechanisms-with-bmv2-p4-switches/-/blob/main/images/Crdt-ISP.png?ref_type=heads) -->

<!-- ## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.


You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser. -->

## Authors and Acknowledgment
I would like to extend our sincere gratitude to those who have contributed to this project. Special thanks to:

Seifeddine Fathalli
Email: fathalli@mpi-inf.mpg.de
Project Supervisor

Thank you for your valuable contributions and support throughout this project. Your guidance and expertise have been invaluable.


<!-- ## License
For open source projects, say how it is licensed. -->

## Project status
Currently, The project is stalling.
